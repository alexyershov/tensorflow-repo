import tensorflow as tf
import valohai as vh
import numpy as np

with np.load(vh.inputs('dataset').path(), allow_pickle=True) as f:
    x_train, y_train = f['x_train'], f['y_train']
    x_test, y_test = f['x_test'], f['y_test']

x_train, x_test = x_train / 255.0, x_test / 255.0

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10)
])

loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=vh.parameters("learning_rate").value),
              loss=loss_fn,
              metrics=['accuracy'])

def log(epoch, logs):
    with vh.logger() as logger:
        logger.log('epoch', epoch)
        logger.log('accuracy', logs['accuracy'])
        logger.log('loss', logs['loss'])


cb = tf.keras.callbacks.LambdaCallback(on_epoch_end=log)

model.fit(x_train, y_train, epochs=vh.parameters("epochs").value, callbacks=[cb])

test_loss, test_acc = model.evaluate(x_test,  y_test, verbose=2)
with vh.logger() as logger:
    logger.log('test_accuracy', test_acc)
    logger.log('test_loss', test_loss)


model.save(vh.outputs("model").path("model"), save_format="tf")


